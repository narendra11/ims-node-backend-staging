module.exports = {
  'env': {
    'browser': true,
    'commonjs': true,
    'es2021': true,
  },
  'extends': [
    'google',
  ],
  'parserOptions': {
    'ecmaVersion': 12,
  },
  'rules': {
    'new-cap': 0,
    'max-len': ['error', {'code': 100}],
    'prefer-rest-params': 0,
    'require-jsdoc': 0,
  },
};
