# Node IMS Backend Staging

```
  Designed and Built by
   ____            _
  / ___|___  _ __ | | _____  __
 | |   / _ \| '_ \| |/ _ \ \/ /
 | |__| (_) | |_) | |  __/>  <
  \____\___/| .__/|_|\___/_/\_\
            |_|
```

## Getting Started

These instructions will get you a copy of the project up and running on your
local machine for development and testing purposes.
See the [deployment](#deployment) section for notes on how to deploy the project
on a live system.

### Prerequisites

To run the Node application, you will need to have the following software installed:
- nvm (node version manager to run different versions of node.js)
- node (node.js javascript runtime)

#### NodeJs

Version v10 or above

##### NodeJs Installation Guides

- [macOS] / [Windows] / [Ubuntu] (https://nodejs.org/en/download/)

#### PostgreSQL

We recommend installing version 9.6 or higher.

When setting up PostgreSQL for the first time, you will need to create a new
user. A commonly used username is `postgres`.

Take note of the username and password as they will be needed to configure the
Node js app. See the [database config](#database-config) section for details.


### Setup

Ensure you have installed all [prerequisites](#prerequisites) above and then:

1. Clone the repository
2. `cd` to project directory
3. Configure [environment variables](#environment-variables)
4. Configure [database](#database-config)
5. Run `make install` to install dependencies
6. Run `npm run migrate` to set up dev databases
7. Run `npm run dev` to start local server
8. Visit http://localhost:3005/api/v1 (base URL of API)

#### Environment variables
To setup application environment variables, copy and rename
`.env.example` to `.env`. (This file is added to
gitignore.)

#### Database Config
Open [`.env`](.env) to see an
example database configuration.

If the example configuration does not work as is, it's likely that you need to
specify the username and password of your PostgreSQL user.
In the `development` and `test` sections, set the `username` and `password`
values to match your user.

Login to postgres as root user

Create postgres user (as in .env file)
`CREATE USER coplex WITH ENCRYPTED PASSWORD 'password';`

Create database
`CREATE DATABASE ims;`
`CREATE DATABASE ims_test;`

Grant privileges
`GRANT ALL PRIVILEGES ON DATABASE ims TO coplex;`
`GRANT ALL PRIVILEGES ON DATABASE ims_test TO coplex;`

## Starting Server

To start the server on the default port of 3005, run the following:

`npm run dev`

## Lint
`npm run lint`
To check code formatting and fix some errors automatically.

## Test
`npm run test`
To run test cases

## Postman collection



## API documentation

