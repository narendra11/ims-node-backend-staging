const request = require('supertest');
const app = require('./../../src/server');
const faker = require('faker');
const {authenticationTokenFactory, companyFactory} = require('../../database/factories/index');
const {CompanyUser, HardCriteria} = require('../../src/models/index');

describe('GET /api/v1/hard_criteria', () => {
  let authenticationToken;
  let company;

  beforeAll(async () => {
    authenticationToken = await authenticationTokenFactory.create();
    company = await companyFactory.create();
    await CompanyUser.create({
      companyId: company.id,
      userId: authenticationToken[2].id,
    });
    hardCriteria = await HardCriteria.create({
      companyId: company.id,
      name: faker.random.word(),
    });
  });

  it('hard criteria index when user is authenticated', async (done) => {
    const response = await request(app)
        .get('/api/v1/hard_criteria')
        .set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer ' + authenticationToken[1]);
    expect(response.statusCode).toBe(201);
    expect(response.body).toEqual(expect.objectContaining([{
      id: expect.any(String),
      company_id: expect.any(String),
      name: expect.any(String),
    }]));
    done();
  });
});
