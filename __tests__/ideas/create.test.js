const request = require('supertest');
const app = require('./../../src/server');
const faker = require('faker');
const {authenticationTokenFactory,
  companyFactory,
  categoryFactory,
} = require('../../database/factories/index');
const {CompanyUser} = require('../../src/models/index');

describe('POST /api/v1/ideas', () => {
  let authenticationToken;
  let company;
  let category;

  beforeAll(async () => {
    authenticationToken = await authenticationTokenFactory.create();
    company = await companyFactory.create();
    await CompanyUser.create({
      companyId: company.id,
      userId: authenticationToken[2].id,
    });
    category = await categoryFactory.create();
  });

  it('new ideas when user is authenticated without file', async (done) => {
    const params = {
      idea: {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        name: faker.random.word(),
        categoryId: category.id,
        description: faker.commerce.productDescription(),
        companyUrl: company.url,
        authKey: company.authKey,
      },
    };
    const response = await request(app)
        .post('/api/v1/ideas')
        .set('Authorization', 'Bearer ' + authenticationToken[1])
        .send(params);
    expect(response.statusCode).toBe(200);
    done();
  });
});
