const request = require('supertest');
const app = require('./../../src/server');
const faker = require('faker');
const {
  authenticationTokenFactory,
  companyFactory,
  categoryFactory,
} = require('../../database/factories/index');
const {CompanyUser, HardCriteria, SoftCriteria, Idea} = require('../../src/models/index');

describe('POST /api/v1/ideas/ideaId/evaluations', () => {
  let authenticationToken;
  let company;
  let category;
  let hardCriteria;
  let softCriteria;
  let idea;

  beforeAll(async () => {
    authenticationToken = await authenticationTokenFactory.create();
    company = await companyFactory.create();
    await CompanyUser.create({
      companyId: company.id,
      userId: authenticationToken[2].id,
    });
    category = await categoryFactory.create();
    hardCriteria = await HardCriteria.create({
      companyId: company.id,
      name: faker.random.word(),
    });
    softCriteria = await SoftCriteria.create({
      companyId: company.id,
      name: faker.random.word(),
      shortName: faker.random.word(),
      weight: 'medium',
    });
    idea = await Idea.create({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      name: faker.random.word(),
      companyId: company.id,
      categoryId: category.id,
      description: faker.commerce.productDescription(),
      companyUrl: company.url,
      authKey: company.authKey,
    });
  });

  it('idea scoring (evaluations) when user is authenticated', async (done) => {
    const metArray = ['true', 'false'];
    const scoreArray = [1, 2, 3, 4, 5];
    const params = {
      hard_criteria: [
        {
          hard_criterium_id: hardCriteria.id,
          met: metArray[Math.floor(Math.random() * metArray.length)],
        },
      ],
      soft_criteria: [
        {
          soft_criterium_id: softCriteria.id,
          score: scoreArray[Math.floor(Math.random() * scoreArray.length)],
        },
      ],
    };
    const response = await request(app)
        .post('/api/v1/ideas/' + idea.id + '/evaluations')
        .set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer ' + authenticationToken[1])
        .send(params);
    expect(response.statusCode).toBe(200);
    done();
  });
});
