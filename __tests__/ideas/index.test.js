const request = require('supertest');
const app = require('./../../src/server');
const faker = require('faker');
const {
  authenticationTokenFactory,
  companyFactory,
  categoryFactory,
} = require('../../database/factories/index');
const {CompanyUser, HardCriteria, SoftCriteria, Idea} = require('../../src/models/index');

describe('GET /api/v1/ideas', () => {
  let authenticationToken;
  let company;
  let category;

  beforeAll(async () => {
    authenticationToken = await authenticationTokenFactory.create();
    company = await companyFactory.create();
    await CompanyUser.create({
      companyId: company.id,
      userId: authenticationToken[2].id,
    });
    category = await categoryFactory.create();
    hardCriteria = await HardCriteria.create({
      companyId: company.id,
      name: faker.random.word(),
    });
    softCriteria = await SoftCriteria.create({
      companyId: company.id,
      name: faker.random.word(),
      shortName: faker.random.word(),
      weight: 'medium',
    });
    idea = await Idea.create({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      name: faker.random.word(),
      companyId: company.id,
      categoryId: category.id,
      description: faker.commerce.productDescription(),
      companyUrl: company.url,
      authKey: company.authKey,
    });
  });

  it('idea scoring (evaluations) index when user is authenticated', async (done) => {
    const response = await request(app)
        .get('/api/v1/ideas?order_by=title&order=asc&page=1&category=all&stage=all')
        .set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer ' + authenticationToken[1]);
    expect(response.statusCode).toBe(200);
    done();
  });
});
