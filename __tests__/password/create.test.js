const request = require('supertest');
const app = require('./../../src/server');
const {userFactory} = require('../../database/factories/index');

describe('POST /api/v1/users/password', () => {
  let user;

  beforeAll(async () => {
    user = await userFactory.create();
  });

  it('#forgot_password when required params are missing', async (done) => {
    const response = await request(app)
        .post('/api/v1/users/password')
        .set('Content-Type', 'application/json')
        .send();

    expect(response.statusCode).toBe(422);
    expect(response.body).toEqual(expect.objectContaining({
      errors: {
        email: expect.arrayContaining(['The Email field is required.']),
      },
    }));
    expect(response.body.message).toEqual('The Email field is required.');
    done();
  });

  it('#forgot_password when password request is successful', async (done) => {
    const params = {
      user: {
        email: user.email,
      },
    };

    const response = await request(app)
        .post('/api/v1/users/password')
        .set('Content-Type', 'application/json')
        .send(params);

    expect(response.statusCode).toBe(200);
    expect(response.body.message)
        .toEqual('Forgot password reset link sent your registered email.');
    done();
  });
});


