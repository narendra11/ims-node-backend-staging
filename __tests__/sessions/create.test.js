const request = require('supertest');
const app = require('./../../src/server');
const faker = require('faker');
const {userFactory} = require('../../database/factories/index');

describe('POST /api/v1/users/sign_in', () => {
  let user;

  beforeAll(async () => {
    user = await userFactory.create();
  });


  it('sign_in when required params are missing', async (done) => {
    const response = await request(app)
        .post('/api/v1/users/sign_in')
        .set('Content-Type', 'application/json')
        .send();

    expect(response.statusCode).toBe(422);
    expect(response.body).toEqual(expect.objectContaining({
      errors: {
        email: expect.arrayContaining(['Email is required']),
        password: expect.arrayContaining(['Password is required']),
      },
    }));
    expect(response.body.message)
        .toEqual('Email is required / Password is required');
    done();
  });

  it('sign_in when authentication is successful', async (done) => {
    const params = {
      user: {
        email: user.email,
        password: user.password,
      },
    };

    const response = await request(app).post('/api/v1/users/sign_in').
        set('User-Agent', faker.internet.userAgent()).
        set('Content-Type', 'application/json').send(params);

    expect(response.statusCode).toBe(201);
    expect(response.body).toEqual(expect.objectContaining({
      auth_token: expect.any(String),
      user: expect.objectContaining({
        id: expect.any(String),
        first_name: expect.any(String),
        last_name: expect.any(String),
        email: expect.any(String),
        phone_number: expect.any(String),
        role: expect.any(String),
        status: expect.any(String),
        slug: expect.any(String),
      }),
    }));
    done();
  });

  it('sign_in when password is invalid', async (done) => {
    const params = {
      user: {
        email: user.email,
        password: faker.internet.password(),
      },
    };

    const response = await request(app)
        .post('/api/v1/users/sign_in')
        .set('Content-Type', 'application/json')
        .send(params);

    expect(response.statusCode).toBe(401);
    expect(response.body.message).toEqual('Invalid email or password.');
    done();
  });
});
