const request = require('supertest');
const app = require('./../../src/server');
const {authenticationTokenFactory} = require('../../database/factories/index');

describe('POST /api/v1/users/sign_out', () => {
  let authenticationToken;

  beforeAll(async () => {
    authenticationToken = await authenticationTokenFactory.create();
  });

  it('sign_out when user is authenticated', async (done) => {
    const response = await request(app).delete('/api/v1/users/sign_out')
        .set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer '+authenticationToken[1])
        .send();
    expect(response.statusCode).toBe(200);
    expect(response.body.message).toEqual('You have successfully signed out.');
    done();
  });
});


