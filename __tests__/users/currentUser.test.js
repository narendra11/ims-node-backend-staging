const request = require('supertest');
const app = require('./../../src/server');
const {authenticationTokenFactory} = require('../../database/factories/index');

describe('POST /api/v1/users/current_user', () => {
  let authenticationToken;

  beforeAll(async () => {
    authenticationToken = await authenticationTokenFactory.create();
  });

  it('current_user when the user is not authenticated', async (done) => {
    const response = await request(app)
        .get('/api/v1/users/current_user')
        .set('Content-Type', 'application/json').send();
    expect(response.statusCode).toBe(401);
    done();
  });

  it('current_user when user is authenticated', async (done) => {
    const response = await request(app).get('/api/v1/users/current_user')
        .set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer ' + authenticationToken[1]);
    expect(response.statusCode).toBe(200);

    expect(response.body).toEqual(expect.objectContaining({
      id: expect.any(String),
      first_name: expect.any(String),
      last_name: expect.any(String),
      email: expect.any(String),
      phone_number: expect.any(String),
      role: expect.any(String),
      status: expect.any(String),
      slug: expect.any(String),
    }));

    done();
  });
});
