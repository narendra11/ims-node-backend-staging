const request = require('supertest');
const faker = require('faker');
const app = require(`./../../src/server`);
const {HttpStatus} = require('./../../src/config/constants');
const {userFactory} = require('../../database/factories/index');

describe('POST /api/v1/users/email_available', () => {
  let user;

  beforeAll(async () => {
    user = await userFactory.create();
  });

  it('when required params are missing', async (done) => {
    const response = await request(app)
        .get('/api/v1/users/email_available')
        .set('Content-Type', 'application/json');

    expect(response.statusCode).toBe(HttpStatus.UNPROCESSABLE_ENTITY);
    expect(response.body).toEqual(expect.objectContaining({
      errors: {
        email: expect.arrayContaining(['The Email field is required.']),
      },
    }));
    expect(response.body.message).toEqual('The Email field is required.');
    done();
  });

  it('when the email is available', async (done) => {
    const response = await request(app)
        .get('/api/v1/users/email_available')
        .set('Content-Type', 'application/json')
        .query({email: user.email});

    expect(response.statusCode).toBe(HttpStatus.CONFLICT);
    expect(response.body.message).toEqual('Email has already been taken');
    done();
  });

  it('when the email is unavailable', async (done) => {
    const response = await request(app).get('/api/v1/users/email_available')
        .set('Content-Type', 'application/json')
        .query({email: faker.internet.email()});
    expect(response.statusCode).toBe(HttpStatus.NO_CONTENT);
    done();
  });
});
