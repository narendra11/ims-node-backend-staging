require('dotenv').config();

const {
  DB_CLIENT,
  DB_DATABASE,
  DB_USERNAME,
  DB_PASSWORD,
  DB_HOST,
  //   DB_PORT,
  TEST_DB_DATABASE,
  TEST_DB_USERNAME,
  TEST_DB_PASSWORD,
  TEST_DB_HOST,
//   TEST_DB_PORT,
} = process.env;

module.exports = {
  development: {
    username: DB_USERNAME,
    password: DB_PASSWORD,
    database: DB_DATABASE,
    host: DB_HOST,
    dialect: DB_CLIENT,
    logging: false,
  },
  test: {
    username: TEST_DB_USERNAME,
    password: TEST_DB_PASSWORD,
    database: TEST_DB_DATABASE,
    host: TEST_DB_HOST,
    dialect: DB_CLIENT,
    logging: false,
  },
  production: {
    'use_env_variable': 'DATABASE_URL',
    'dialect': 'postgres',
    'dialectOptions': {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
  },
};
