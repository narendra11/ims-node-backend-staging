const faker = require('faker');
const {AuthenticationToken} = require('../../src/models/index');
const userFactory = require('./userFactory');
const jwt = require('../../src/libs/jwt');

async function create() {
  const user = await userFactory.create();
  const token = await jwt.accessToken(user.toJSON());
  const auth = await AuthenticationToken.create({
    body: token,
    userId: user.get('id'),
    ipAddress: '127.32.35.1',
    userAgent: faker.internet.userAgent(),
    expiresIn: 3600,
  });
  return [auth, token, user];
}

module.exports = {
  create,
};
