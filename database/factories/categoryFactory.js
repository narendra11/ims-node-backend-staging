const faker = require('faker');
const {Category} = require('../../src/models/index');

async function create() {
  const category = {
    name: faker.random.word(),
  };
  return await Category.create(category);
}

module.exports = {
  create,
};
