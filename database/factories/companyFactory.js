const faker = require('faker');
const {Company} = require('../../src/models/index');

async function create() {
  const company = {
    name: faker.name.firstName(),
    url: faker.internet.url(),
    password: 'password',
    authKey: faker.random.word(),
  };
  return await Company.create(company);
}

module.exports = {
  create,
};
