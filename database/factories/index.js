const userFactory = require('./userFactory');
const authenticationTokenFactory = require('./authenticationTokenFactory');
const companyFactory = require('./companyFactory');
const categoryFactory = require('./categoryFactory');

module.exports = {
  userFactory, authenticationTokenFactory, companyFactory, categoryFactory,
};
