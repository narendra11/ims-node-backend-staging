const faker = require('faker');
const {User} = require('../../src/models/index');

const firstName = faker.name.firstName();
const lastName = faker.name.lastName();
const email = faker.internet.email();
const password = faker.internet.password();
const phoneNumber = faker.phone.phoneNumber('0165#######');

async function create() {
  const user = {
    firstName: firstName,
    lastName: lastName,
    email: email,
    password: password,
    phoneNumber: phoneNumber,
  };
  return await User.create(user);
}

async function createAdminRole() {
  const user = {
    firstName: firstName,
    lastName: lastName,
    email: email,
    password: password,
    phoneNumber: phoneNumber,
    role: 'admin',
  };
  return await User.create(user);
}

module.exports = {
  create, createAdminRole,
};
