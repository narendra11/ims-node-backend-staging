'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Companies', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      url: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true,
      },
      encryptedPassword: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      encryptedPasswordIv: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      submissionPasswordRequired: {
        allowNull: true,
        type: Sequelize.BOOLEAN,
      },
      setupComplete: {
        allowNull: true,
        type: Sequelize.BOOLEAN,
      },
      authKey: {
        allowNull: true,
        type: Sequelize.STRING,
        unique: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Companies');
  },
};
