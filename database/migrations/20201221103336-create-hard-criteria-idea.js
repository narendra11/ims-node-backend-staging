'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('HardCriteriaIdeas', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      ideaId: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'Ideas',
          key: 'id',
        },
      },
      hardCriteriaId: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'HardCriteria',
          key: 'id',
        },
      },
      met: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('HardCriteriaIdeas');
  },
};
