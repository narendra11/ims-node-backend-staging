'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('IdeaSoftCriteria', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      ideaId: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'Ideas',
          key: 'id',
        },
      },
      softCriteriaId: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'SoftCriteria',
          key: 'id',
        },
      },
      score: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('IdeaSoftCriteria');
  },
};
