const {Category} = require('../../models');
const {HttpStatus} = require('../../config/constants');
const categoriesIndexView = require('../../views/categories/index');

async function index(request, response, next) {
  try {
    const categories = await Category.findAll();

    return response.status(HttpStatus.OK).json(categoriesIndexView(categories));
  } catch (e) {
    next(e);
  }
}

module.exports = {
  index,
};
