const {
  Company,
  HardCriteria,
  SoftCriteria,
  Category,
  Idea,
  User,
} = require('../../models');
const {HttpStatus} = require('../../config/constants');
require('dotenv').config();
const companyAuthCheckView = require('../../views/companies/authCheck');
const companyAuthView = require('../../views/companies/auth');

async function update(request, response, next) {
  try {
    const company = await findCompany(request.params.id);
    company.update({
      setupComplete: request.body.company.setupComplete,
      submissionPasswordRequired: request.body.company.submission_password_required,
      name: request.body.company.name,
    });

    return response.status(HttpStatus.OK).render('companies/company', {data: company});
  } catch (e) {
    next(e);
  }
}

async function me(request, response, next) {
  try {
    const user = await User.findByPk(request.user.id, {
      include: {
        model: Company,
        as: 'companies',
        include: [{
          model: HardCriteria,
          as: 'hardCriteria',
        },
        {
          model: SoftCriteria,
          as: 'softCriteria',
        }],
      },
    });

    return response.status(HttpStatus.OK).json(user.companies[0]);
  } catch (e) {
    next(e);
  }
}

async function dashboard(request, response, next) {
  try {
    const user = await User.findByPk(request.user.id, {
      include: {
        model: Company,
        as: 'companies',
        include: {
          model: Idea,
          as: 'ideas',
          include: {
            model: Category,
            as: 'category',
          },
        },
      },
    });
    const company = user.companies[0];

    const populateDashboard = company.ideas.length > 2;
    const ideasByStage = company.ideasByStage();
    const ideasByCategory = company.ideasByCategory();
    const ideasByTime = company.ideasByTime();

    const result = {
      populateDashboard: populateDashboard,
      stages: ideasByStage,
      categories: ideasByCategory,
      ideas: ideasByTime,
    };

    return response.status(HttpStatus.OK).json(result);
  } catch (e) {
    next(e);
  }
}

async function authCheck(request, response, next) {
  try {
    const company = await findCompanyByUrl(request.params.companyUrl);

    return response.status(HttpStatus.OK).json(companyAuthCheckView(company));
  } catch (e) {
    next(e);
  }
}

async function auth(request, response, next) {
  try {
    const company = await findCompanyByUrl(request.params.companyUrl);
    if (request.body.idea.password != company.password) {
      return response.status(HttpStatus.UNAUTHORIZED)
          .json({message: 'Invalid password'});
    }

    return response.status(HttpStatus.OK).json(companyAuthView(company));
  } catch (e) {
    next(e);
  }
}

// Local methods
async function findCompany(id) {
  const company = await Company.findOne({
    where: {id: id},
  });
  if (!company) throw new Error('CUSTOM_NOT_FOUND');
  return company;
}

async function findCompanyByUrl(url) {
  const company = await Company.findOne({
    where: {url: url},
  });
  if (!company) throw new Error('CUSTOM_NOT_FOUND');
  return company;
}

module.exports = {
  update, me, dashboard, authCheck, auth,
};
