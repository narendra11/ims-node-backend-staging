require('dotenv').config();
const {FeatureRequest} = require('../../models');
const {HttpStatus} = require('../../config/constants');
const featureRequestView = require('../../views/featureRequests/featureRequest');

async function create(request, response, next) {
  try {
    const featureRequest = await FeatureRequest.create({
      ...request.body.featureRequest,
      userId: request.user.id,
    }, {fields: ['id', 'subject', 'description', 'userId']});

    return response.status(HttpStatus.CREATED).json(featureRequestView(featureRequest));
  } catch (e) {
    next(e);
  }
}

module.exports = {
  create,
};
