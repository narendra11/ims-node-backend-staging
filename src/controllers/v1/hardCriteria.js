const {User, HardCriteria, Company} = require('../../models');
const {HttpStatus} = require('../../config/constants');
const hardCriteriaIndexView = require('../../views/hardCriteria/index');
require('dotenv').config();

async function create(request, response, next) {
  try {
    const user = await User.findByPk(request.user.id, {include: 'companies'});

    await HardCriteria.destroy({where: {companyId: user.companies[0].id}});

    const hardCriteriaParams = request.body.hardCriteria.map((hardCriterium) => {
      return {...hardCriterium, companyId: user.companies[0].id};
    });
    const hardCriteria = await HardCriteria.bulkCreate(hardCriteriaParams, {validate: true});

    return response.status(HttpStatus.CREATED).json(hardCriteriaIndexView(hardCriteria));
  } catch (e) {
    next(e);
  }
}

async function index(request, response, next) {
  try {
    const user = await User.findByPk(request.user.id, {
      include: {
        model: Company,
        as: 'companies',
        include: {
          model: HardCriteria,
          as: 'hardCriteria',
        },
      },
    });

    return response.status(HttpStatus.CREATED)
        .json(hardCriteriaIndexView(user.companies[0].hardCriteria));
  } catch (e) {
    next(e);
  }
}

module.exports = {
  create, index,
};
