const {
  Idea,
  Company,
  HardCriteria,
  SoftCriteria,
  HardCriteriaIdea,
  IdeaSoftCriteria,
  Category,
  Attachment,
  User,
} = require('../../models');
const {HttpStatus} = require('../../config/constants');
require('dotenv').config();
const ideasIndexView = require('../../views/ideas/index');
const ideaView = require('../../views/ideas/idea');
const evaluationsView = require('../../views/ideas/evaluations');
const evaluationView = require('../../views/ideas/evaluation');
const Op = require('sequelize').Op;

async function create(req, res, next) {
  try {
    const company = await authenticateCompany(
        req.body.idea.companyUrl,
        req.body.idea.authKey,
    );

    if (!company) {
      return res.status(HttpStatus.UNAUTHORIZED)
          .json({message: 'Invalid company password or auth key.'});
    }

    let idea = await Idea.create({
      ...req.body.idea,
      companyId: company.id,
    });
    // file upload starts
    if (req.body.idea.files) {
      const files = req.body.idea.files;
      for (let i=0; i<files.length; i++) {
        const file = files[i];
        await idea.createFile({
          fileName: file.originalname,
          url: file.location,
          contentType: file.mimetype,
          byteSize: file.size,
        });
      }
    }
    // file upload ends
    idea = await idea.reload({include: ['company', 'category', 'files']});
    return res.status(HttpStatus.OK).json(ideaView(idea));
  } catch (e) {
    next(e);
  }
}

async function unscored(request, response, next) {
  try {
    const limit = 10;
    const page = request.query.page - 1;
    const condition = {
      limit: limit,
      offset: page * limit,
      include: ['company', 'category', 'files'],
      where: {score: null},
      distinct: true,
    };
    condition.order = orderClause(request.query.order_by, request.query.order);

    const ideas = await Idea.findAndCountAll(condition);
    ideas.pageCount = Math.ceil(ideas.count / limit);

    return response.status(HttpStatus.OK).json(ideasIndexView(ideas));
  } catch (e) {
    next(e);
  }
}

async function index(request, response, next) {
  try {
    const limit = 10;
    const page = request.query.page - 1;
    const condition = {
      limit: limit,
      offset: page * limit,
      include: ['company', 'category', 'files'],
      where: {
        [Op.not]: [
          {score: null},
        ],
      },
      distinct: true,
    };
    condition.order = orderClause(request.query.order_by, request.query.order);
    // filters
    if (request.query.category != 'all') condition.where.categoryId = request.query.category;
    if (request.query.stage != 'all') {
      condition.where.stage = Idea.stages().indexOf(request.query.stage);
    }

    const ideas = await Idea.findAndCountAll(condition);
    ideas.pageCount = Math.ceil(ideas.count / limit);

    return response.status(HttpStatus.OK).json(ideasIndexView(ideas));
  } catch (e) {
    next(e);
  }
}

async function evaluations(request, response, next) {
  try {
    const idea = await Idea.findByPk(request.params.id, {
      include: [
        {
          model: Category,
          as: 'category',
        },
        {
          model: Attachment,
          as: 'files',
        },
      ],
    });
    const user = await User.findByPk(request.user.id, {
      include:
        {
          model: Company,
          as: 'companies',
          include: [
            {
              model: HardCriteria,
              as: 'hardCriteria',
              include: {
                model: HardCriteriaIdea,
                as: 'hardCriteriaIdeas',
                where: {ideaId: idea.id},
                required: false,
              },
            },
            {
              model: SoftCriteria,
              as: 'softCriteria',
              include: {
                model: IdeaSoftCriteria,
                as: 'ideaSoftCriteria',
                where: {ideaId: idea.id},
                required: false,
              },
            },
          ],
        },

    });
    return response.status(HttpStatus.OK).json(evaluationsView(idea, user.companies[0]));
  } catch (e) {
    next(e);
  }
}

async function evaluation(request, response, next) {
  try {
    const idea = await Idea.findByPk(request.params.id, {
      include: [
        {
          model: HardCriteriaIdea,
          as: 'hardCriteriaIdea',
        },
        {
          model: IdeaSoftCriteria,
          as: 'ideaSoftCriteria',
        },
        {
          model: Category,
          as: 'category',
        },
        {
          model: Attachment,
          as: 'files',
        },
      ],
    });
    await HardCriteriaIdea.destroy({where: {ideaId: idea.id}});
    await IdeaSoftCriteria.destroy({where: {ideaId: idea.id}});

    await idea.update({
      stage: null,
      score: null,
    });

    await hardCriteriaFunction(request.body.hardCriteria, idea);
    await softCriteriaFunction(request.body.softCriteria, idea);

    await idea.update({
      notes: request.body.notes,
      favorite: request.body.favorite,
    });

    return response.status(HttpStatus.OK).json(evaluationView(idea));
  } catch (e) {
    next(e);
  }
}

async function update(request, response, next) {
  try {
    let idea = await Idea.findByPk(id);
    if (!idea) throw new Error('CUSTOM_NOT_FOUND');

    idea.update(request.body.idea, {
      fields: ['favorite', 'stage'],
    });
    idea = await idea.reload({include: ['company', 'category', 'files']});
    return response.status(HttpStatus.OK).json(ideaView(idea));
  } catch (e) {
    next(e);
  }
}

// Local methods
async function hardCriteriaFunction(hardCriteriaRequest, idea) {
  const stageValue = [];
  const hardCriteriaIdeaAttributes = hardCriteriaRequest.map((hardCriteria) => {
    stageValue.push(hardCriteria.met);
    return {
      ideaId: idea.id,
      hardCriteriaId: hardCriteria.hardCriteriumId,
      met: hardCriteria.met,
    };
  });
  await HardCriteriaIdea.bulkCreate(hardCriteriaIdeaAttributes, {validate: true});

  const stage = stageValue.includes('false') ? 'Disqualified' : 'Unstarted';
  idea.update({stage: stage});
}

async function softCriteriaFunction(softCriteriaRequest, idea) {
  let totalPoints = totalPointsPossible = 0;
  const softCriteriaIdeaAttributes = softCriteriaRequest.map((softCriteria) => {
    return {
      ideaId: idea.id,
      softCriteriaId: softCriteria.softCriteriumId,
      score: softCriteria.score,
    };
  });
  await IdeaSoftCriteria.bulkCreate(softCriteriaIdeaAttributes, {validate: true});

  for (let i=0; i<softCriteriaIdeaAttributes.length; i++) {
    const softCriterium = await SoftCriteria.findByPk(softCriteriaIdeaAttributes[i].softCriteriaId);

    const weightValue = SoftCriteria.weightValue(softCriterium.weight);

    totalPoints += weightValue * softCriteriaIdeaAttributes[i].score;
    totalPointsPossible += weightValue * 5;
  }
  const scoring = Math.round((totalPoints / totalPointsPossible) * 100);
  await idea.update({
    score: scoring,
  });
}

async function authenticateCompany(url, authKey) {
  const company = await Company.findOne({where: {url: url}});
  if (!company) throw new Error('CUSTOM_NOT_FOUND');

  return (!company.submissionPasswordRequired || company.authenticate(authKey)) ?
    company :
    null;
}

function orderClause(orderBy, orderDirection) {
  const orderFields = [];

  if (orderBy == 'date') {
    orderFields.push('createdAt');
  } else if (orderBy == 'title') {
    orderFields.push('name');
  } else if (orderBy == 'category') {
    orderFields.push(...['category', 'name']);
  } else if (orderBy == 'stage') {
    orderFields.push('stage');
  } else if (orderBy == 'submitted_by') {
    orderFields.push('firstName');
  } else if (orderBy == 'score') {
    orderFields.push('score');
  }

  orderFields.push(orderDirection);
  return [orderFields];
}

module.exports = {
  create, unscored, index, evaluations, evaluation, update,
};
