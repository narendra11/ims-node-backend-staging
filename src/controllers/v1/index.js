const users = require('./users');
const contactMessages = require('./contact_messages');
const installations = require('./installations');
const password = require('./password');
const sessions = require('./sessions');
const slowRequestLogs = require('./slow_request_logs');
const comments = require('./comments');
const tags = require('./tags');
const taggings = require('./taggings');
const hardCriterias = require('./hard_criterias');
const softCriterias = require('./soft_criterias');
const companies = require('./companies');
const ideas = require('./ideas');
const featureRequests = require('./feature_requests');

module.exports = {
  users, contactMessages, sessions, password, installations,
  slowRequestLogs, comments, tags, taggings, hardCriterias, softCriterias,
  companies, ideas, featureRequests,
};
