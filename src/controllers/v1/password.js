const {User} = require('../../models');
const {HttpStatus, UserMessage} = require('../../config/constants');
const jwt = require('../../libs/jwt');
const utils = require('../../libs/utils');
require('dotenv').config();
const mail = require('../../libs/mail');

async function forgotPassword(request, response, next) {
  try {
    const user = await findUser(request.body.user.email);
    const randomCode = await utils.randomText(6);
    const resetToken = await jwt.encode({code: randomCode, id: user.id});
    await user.update({resetPasswordToken: resetToken, resetPasswordSentAt: new Date()});
    mail.forgotPassword(user, resetToken, request);
    return response.status(HttpStatus.OK).json({message: UserMessage.FORGOT_PASSWORD_MSG});
  } catch (e) {
    next(e);
  }
}

async function resetPassword(request, response, next) {
  try {
    await jwt.verify(request.body.user.resetPasswordToken);
    const user = await findResetPassword(request.body.user.resetPasswordToken);

    await user.update({
      password: request.body.user.password,
      reset_password_token: null,
      reset_password_sent_at: null,
    });
    return response.status(HttpStatus.OK).json({message: UserMessage.PASSWORD_UPDATED});
  } catch (e) {
    next(e);
  }
}

async function findResetPassword(token) {
  const user = await User.findOne(
      {where: {resetPasswordToken: token}},
  );
  if (!user) {
    throw new Error('CUSTOM_NOT_FOUND');
  }
  return user;
}

async function findUser(email) {
  const user = await User.findOne({
    where: {email: email}},
  );

  if (!user) {
    throw new Error('CUSTOM_NOT_FOUND');
  }
  return user;
}

module.exports = {
  forgotPassword,
  resetPassword,
};
