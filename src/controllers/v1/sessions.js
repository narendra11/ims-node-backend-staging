const {User, AuthenticationToken, Company, Idea} = require('../../models');
const {HttpStatus, UserMessage} = require('../../config/constants');
const jwt = require('../../libs/jwt');
const utils = require('../../libs/utils');
const authenticationTokenView = require('../../views/authenticationTokens/authenticationToken');

async function create(request, response, next) {
  try {
    const user = await User.findOne({where: {email: request.body.user.email}});

    if (user && await utils.hashCompare(request.body.user.password, user.encryptedPassword)) {
      const token = await jwt.accessToken(user.toJSON());
      const ipAddress = request.headers['x-forwarded-for'] || request.connection.remoteAddress;
      const authenticationToken = await AuthenticationToken.create({
        body: token,
        userId: user.id,
        ipAddress: ipAddress,
        userAgent: request.get('user-agent'),
        expiresIn: 3600,
      });

      await authenticationToken.reload({
        include: {
          model: User,
          as: 'user',
          include: {
            model: Company,
            as: 'companies',
            include: {
              model: Idea,
              as: 'ideas',
              where: {score: null},
              required: false,
            },
          },
        },
      });

      return response.status(HttpStatus.CREATED).json(authenticationTokenView(authenticationToken));
    } else {
      return response.status(HttpStatus.UNAUTHORIZED).json({message: UserMessage.INVALID_LOGIN});
    }
  } catch (e) {
    next(e);
  }
}

async function destroy(request, response, next) {
  try {
    const token = request.headers['x-user-token'] || request.user.access_token;
    const authToken = await findAuth(token, request.user.id);
    await authToken.destroy();

    return response.status(HttpStatus.OK).json({message: UserMessage.LOGOUT});
  } catch (e) {
    next(e);
  }
}

// Local methods
async function findAuth(token, userId) {
  const authToken = await AuthenticationToken.findOne({
    where: {body: token, userId: userId},
  });
  if (!authToken) {
    throw new Error('CUSTOM_NOT_FOUND');
  }
  return authToken;
}

module.exports = {
  create,
  destroy,
};
