require('dotenv').config();
const {SoftCriteria, User, Company} = require('../../models');
const {HttpStatus} = require('../../config/constants');
const softCriteriaIndexView = require('../../views/softCriteria/index');

async function create(request, response, next) {
  try {
    const user = await User.findByPk(request.user.id, {include: 'companies'});

    await SoftCriteria.destroy({where: {companyId: user.companies[0].id}});

    const softCriteriaParams = request.body.softCriteria.map((softCriterium) => {
      return {...softCriterium, companyId: user.companies[0].id};
    });
    const softCriteria = await SoftCriteria.bulkCreate(softCriteriaParams, {validate: true});

    return response.status(HttpStatus.CREATED).json(softCriteriaIndexView(softCriteria));
  } catch (e) {
    next(e);
  }
}

async function index(request, response, next) {
  try {
    const user = await User.findByPk(request.user.id, {
      include: {
        model: Company,
        as: 'companies',
        include: {
          model: SoftCriteria,
          as: 'softCriteria',
        },
      },
    });

    return response.status(HttpStatus.CREATED)
        .json(softCriteriaIndexView(user.companies[0].softCriteria));
  } catch (e) {
    next(e);
  }
}

module.exports = {
  create, index,
};
