const {User, Company, Idea} = require('../../models');
const {HttpStatus} = require('../../config/constants');
const utils = require('../../libs/utils');
require('dotenv').config();
const userView = require('../../views/users/user');
const userUpdateView = require('../../views/users/update');

async function update(request, response, next) {
  try {
    const user = await findUser(request.user.id);

    if (('password' in request.body.user)) {
      const passwordMatches = await utils.hashCompare(
          request.body.user.currentPassword,
          user.encryptedPassword,
      );
      if (!passwordMatches) {
        return response.status(HttpStatus.UNAUTHORIZED)
            .json({message: 'Invalid current password'});
      }
    }

    if (('setupComplete' in request.body.user)) {
      if (!user.company) {
        return response.status(HttpStatus.NOT_FOUND)
            .json({message: 'Company not linked'});
      }
      const company = await Company.findByPk(request.body.user.companyId);
      await company.update({setupComplete: request.body.user.setupComplete});
    }

    // await user.update(request.body.user);
    await user.update({
      firstName: request.body.user.firstName,
      lastName: request.body.user.lastName,
      email: request.body.user.email,
    });
    const updatedUser = await findUser(request.user.id);

    return response.status(HttpStatus.OK)
        .json(userUpdateView(updatedUser, utils.tokenFromRequest(request)));
  } catch (e) {
    next(e);
  }
}

async function current(request, response, next) {
  try {
    const user = await User.findByPk(request.user.id, {
      include: {
        model: Company,
        as: 'companies',
        include: {
          model: Idea,
          as: 'ideas',
          where: {score: null},
          required: false,
        },
      },
    });

    return response.status(HttpStatus.OK).json(userView(user, user.companyIdeas.length));
  } catch (e) {
    next(e);
  }
}

async function emailAvailable(request, response, next) {
  const user = await User.findOne({where: {email: request.query.email}});
  if (!user) return response.status(HttpStatus.NO_CONTENT).json();

  return response.status(HttpStatus.CONFLICT)
      .json({message: 'Email has already been taken'});
}

async function findUser(id) {
  const user = await User.findOne({
    where: {id: id},
    include: 'companies',
  });

  if (!user) throw new Error('CUSTOM_NOT_FOUND');

  return user;
}

module.exports = {
  update,
  current,
  emailAvailable,
};
