const {HttpStatus, AuthMessage, ErrorMessage} = require('../config/constants');
require('dotenv').config();
const Sentry = require('@sentry/node');
const _ = require('lodash');
// const Tracing = require("@sentry/tracing");
Sentry.init({
  dsn: process.env.SENTRY_KEY,
  tracesSampleRate: 1.0,
});

const validationMessage = (error) => {
  let message = error.message;

  switch (error.validatorKey) {
    case 'notEmpty':
    case 'is_null':
      message = `${_.startCase(error.path)} is required`;
      break;
  }

  return message;
};


const validationErrors = (error) => {
  const errors = {};
  const messages = [];

  error.errors.forEach((e) => {
    const message = validationMessage(e);

    messages.push(message);

    if (errors.hasOwnProperty(e.path)) {
      errors[e.path].push(message);
    } else {
      errors[e.path] = [message];
    }
  });

  const fullMessage = messages.join(' / ');

  return {
    errors: errors,
    message: fullMessage,
  };
};

exceptionHandler = async (error, req, res, next) => {
  console.log(error);

  let responseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
  const responseBody = {
    message: 'Internal Server Error',
  };

  if (error.message == 'CUSTOM_NOT_FOUND') {
    responseStatus = HttpStatus.NOT_FOUND;
    responseBody.message = ErrorMessage.NO_RECORDS_FOUND;
    res.status(responseStatus).send(responseBody);
  }
  // When wrong length UUID is supplied to query
  if ((error.name == 'SequelizeDatabaseError') && (error.parent.code == '22P02')) {
    responseStatus = HttpStatus.UNPROCESSABLE_ENTITY;
    responseBody.message = 'Valid Id is required';
    res.status(responseStatus).send(responseBody);
  }
  switch (error.name) {
    case 'SequelizeValidationError':
      const validationResponse = validationErrors(error);
      responseStatus = HttpStatus.UNPROCESSABLE_ENTITY;
      responseBody.errors = validationResponse.errors;
      responseBody.message = validationResponse.message;
      break;
    case 'SequelizeUniqueConstraintError':
      const uniqueValidationResponse = validationErrors(error);
      responseStatus = HttpStatus.UNPROCESSABLE_ENTITY;
      responseBody.errors = uniqueValidationResponse.errors;
      responseBody.message = uniqueValidationResponse.message;
      break;
    case 'TokenExpiredError':
      responseStatus = HttpStatus.UNAUTHORIZED;
      responseBody.message = AuthMessage.TOKEN_EXPIRED;
      break;
    case 'JsonWebTokenError':
      responseStatus = HttpStatus.UNAUTHORIZED;
      responseBody.message = AuthMessage.TOKEN_EXPIRED;
      break;
    case 'CustomError':
      responseStatus = HttpStatus.NOT_FOUND;
      responseBody.message = ErrorMessage.NO_RECORDS_FOUND;
      break;
    case 'error':
      responseBody.message = error.message || error.error;
      break;
  }
  if (process.env.NODE_ENV == 'production') {
    Sentry.captureException(error);
  }
  res.status(responseStatus).send(responseBody);
};

clientHandler = async (req, res, next) => {
  // 400 errors
  res.status(HttpStatus.NOT_FOUND).send({
    status: HttpStatus.NOT_FOUND,
    error: 'Not found',
  });

  // 500 errors
//   res.status(res.status || HttpStatus.INTERNAL_SERVER_ERROR).send({
//     status: res.status || HttpStatus.INTERNAL_SERVER_ERROR,
//     message: res.message || 'Internal Server Error',
//   });
};


module.exports = {
  exceptionHandler, clientHandler,
};
