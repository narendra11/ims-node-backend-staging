const jwt = require('jsonwebtoken');

const TOKEN_EXPIRED = '2d';
const ALGORITHM = 'RS256';
const EXPIRY = 86400; // 24 hours

async function accessToken(payload) {
  return await jwt.sign(payload, process.env.JWT_SECRET, {expiresIn: TOKEN_EXPIRED});
}

async function verify(token) {
  return await jwt.verify(token.trim(), process.env.JWT_SECRET, ALGORITHM);
}

async function decode(token) {
  return await jwt.verify(token, process.env.JWT_SECRET, ALGORITHM);
}

async function encode(payload, expiredTime=EXPIRY) {
  return await jwt.sign(payload, process.env.JWT_SECRET, {expiresIn: expiredTime}, ALGORITHM);
}

module.exports = {
  accessToken, verify, decode, encode,
};
