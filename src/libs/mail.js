require('dotenv').config();
const nodeMailerUtil = require('./nodeMailer');
const path = require('path');
const handlebars = require('handlebars');
const sendEmail = require('./sendEmail');

async function forgotPassword(user, resetToken, request) {
  const subject = 'Forgot Password';
  await nodeMailerUtil.readHTMLFile(
      path.join(__dirname, '../views/mailer/forgot_password.html'), function(err, html) {
        const template = handlebars.compile(html);
        const replacements = {
          first_name: user.firstName,
          // processEnv: process.env.WEB_SERVER,
          reset_password_token: resetToken,
        };
        const htmlToSend = template(replacements);
        const mailOptions = {
          from: 'narendra@coplex.com',
          to: request.body.user.email,
          subject: subject,
          html: htmlToSend,
        };
        sendEmail.sendEmail(mailOptions);
      });
}

async function contactMessage(request, adminEmails) {
  const subject = 'New contact message';
  await nodeMailerUtil.readHTMLFile(
      path.join(__dirname, '../views/mailer/contact.html'), function(err, html) {
        const template = handlebars.compile(html);
        const replacements = {
          first_name: request.firstName,
          last_name: request.lastName,
          email: request.email,
          reason: request.reason,
          message: request.message,
        };
        const htmlToSend = template(replacements);
        const mailOptions = {
          from: process.env.FROM_MAIL,
          to: adminEmails.join(', '), // send to all admins
          subject: subject,
          html: htmlToSend,
        };
        sendEmail.sendEmail(mailOptions);
      });
}

module.exports = {
  forgotPassword, contactMessage,
};
