const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

/**
 * send mail with text/html request body.
 * @param {string} to email address to send mail.
 * @param {string} subject mail subject.
 * @param {string} messageBodyText mail plain text body.
 * @param {string} messageBodyHtml mail html body.
 * @return {boolean} response reponse .
 */
async function sendMail(to, subject, messageBodyText, messageBodyHtml) {
  const msg = {
    to: to,
    from: process.env.FROM_MAIL,
    subject: subject,
  };
  msg.text = messageBodyText;
  msg.html = messageBodyHtml;
  await sgMail.send(msg);
}


module.exports = {
  sendMail,
};
