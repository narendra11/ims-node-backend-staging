const nodemailer = require('nodemailer');
const fs = require('fs');
require('dotenv').config();

const readHTMLFile = function(path, next) {
  fs.readFile(path, {encoding: 'utf-8'}, function(err, html) {
    if (err) {
      next(err);
    } else {
      next(null, html);
    }
  });
};

const transportOnBrowser = nodemailer.createTransport({
  jsonTransport: true,
});

const transporter = nodemailer.createTransport({
  service: 'sendgrid',
  auth: {
    user: process.env.SENDGRID_USERNAME,
    pass: process.env.SENDGRID_API_KEY,
  },
});

module.exports = {
  readHTMLFile, transportOnBrowser, transporter,
};
