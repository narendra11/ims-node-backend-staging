require('dotenv').config();
const nodeMailerUtil = require('./nodeMailer');
const previewEmail = require('preview-email');

async function sendEmail(mailOptions) {
  if (process.env.NODE_ENV !== 'test') {
    if (process.env.NODE_ENV == 'development') { // mail not send and automatically opens on browser
      previewEmail(mailOptions);
      nodeMailerUtil.transportOnBrowser.sendMail(mailOptions);
    } else { // mail sends
      nodeMailerUtil.transporter.sendMail(mailOptions, function(error, response) {
        if (error) {
          next(error);
        }
      });
    }
  }
}

module.exports = {
  sendEmail,
};
