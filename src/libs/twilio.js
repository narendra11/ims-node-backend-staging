require('dotenv').config();
const twilio = require('twilio');

async function sendSMS({phoneNumber, message}) {
  if (!phoneNumber || process.env.NODE_ENV == 'test') return false;

  const twilioClient = new twilio(
      process.env.TWILIO_ACCOUNT_SID,
      process.env.TWILIO_AUTH_TOKEN,
  );

  twilioClient.messages.create({
    body: message,
    to: phoneNumber,
    from: process.env.TWILIO_NUMBER, // From a valid Twilio number
  })
      .then((success) => console.log(success))
      .catch((error) => console.log(error));
}

module.exports = {
  sendSMS,
};
