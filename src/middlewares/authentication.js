const {HttpStatus, AuthMessage} = require('../config/constants');
const {AuthenticationToken} = require('../models');
const jwt = require('../libs/jwt');
const utils = require('../libs/utils');

module.exports = async (req, res, next) => {
  const token = utils.tokenFromRequest(req);

  if (!token) {
    return res.status(HttpStatus.UNAUTHORIZED).send({
      message: AuthMessage.TOKEN_NOT_FOUND,
    });
  }

  try {
    // verify makes sure that the token hasn't expired and has been issued by us
    result = await jwt.verify(token);
    if (result==null) {
      return res.status(HttpStatus.UNAUTHORIZED).send({
        message: AuthMessage.TOKEN_EXPIRED,
      });
    }

    if (!await AuthenticationToken.verifyAuthToken({body: token, user_id: result.id})) {
      return res.status(HttpStatus.UNAUTHORIZED).send({
        message: AuthMessage.INVALID_TOKEN,
      });
    }
    // Let's pass back the decoded token to the request object
    req.user = result;
    req.user.access_token = token;
    // We call next to pass execution to the subsequent middleware
    next();
  } catch (err) {
    // Throw an error just in case anything goes wrong with verification
    next(err);
  }
};
