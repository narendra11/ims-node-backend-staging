'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Attachment extends Model {
    static associate(models) {
      models.Attachment.belongsTo(models.Idea, {
        foreignKey: 'attachableId',
        onDelete: 'CASCADE',
        constraints: false,
      });
    }
  };
  Attachment.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    attachableId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    attachableType: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    fileName: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    contentType: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    byteSize: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  }, {
    sequelize,
    modelName: 'Attachment',
  });
  return Attachment;
};
