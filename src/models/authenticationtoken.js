'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class AuthenticationToken extends Model {
    /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         * @param {models} models model.
         */
    static associate(models) {
      // define association here
      models.AuthenticationToken.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        as: 'user',
      });
    };
    static verifyAuthToken(params) {
      const authToken = AuthenticationToken.findOne(
          {where: {id: params.user_id, body: params.body}},
      );
      if (authToken) {
        return true;
      }
      return false;
    };
  };
  AuthenticationToken.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    userId: DataTypes.UUID,
    body: DataTypes.TEXT,
    ipAddress: DataTypes.STRING,
    userAgent: DataTypes.STRING,
    expiresIn: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'AuthenticationToken',
  });

  return AuthenticationToken;
};
