'use strict';
const {v4: uuidv4} = require('uuid');
const {encrypt, decrypt} = require('../libs/crypto');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Company extends Model {
    authenticate(authKey) {
      return this.authKey == authKey;
    }

    ideasByStage() {
      // This works when ideas are eagerly loaded
      // TODO: Load relationship in instance method
      const totalIdeasCount = this.ideas.length;
      const stages = [];
      sequelize.model('Idea').stages().forEach((stage) => {
        const ideasCount = this.ideas.filter((idea) => idea.stage == stage).length;
        const percent = totalIdeasCount > 0 ?
          ((parseFloat(ideasCount) / 100) / totalIdeasCount) :
          0;
        stages.push({name: stage, value: Math.round(percent)});
      });

      return stages;
    }

    ideasByCategory() {
      const categories = [];
      this.ideas.forEach((idea) => {
        const categoryName = idea.category.name;
        const category = categories.find((category) => category.name === categoryName);
        if (category) {
          category.value += 1;
        } else {
          categories.push({name: categoryName, value: 1});
        }
      });

      return categories;
    }

    ideasByTime() {
      const scoredIdeas = this.ideas.filter((idea) => !!idea.score);

      const ideasData = [];

      for (let i = 1; i < 6; i++) {
        const now = new Date();
        const previousMonth = new Date(now.setMonth(now.getMonth() - i));
        const monthStart = new Date(previousMonth.getFullYear(), previousMonth.getMonth(), 1);
        const monthEnd = new Date(previousMonth.getFullYear(), previousMonth.getMonth() + 1, 0);

        const ideasInRange = scoredIdeas.filter((idea) => {
          return (idea.createdAt >= monthStart) && (idea.createdAt <= monthEnd);
        });
        const ideasCount = ideasInRange.length;
        const averageScore = ideasCount > 0 ?
          ideasInRange.reduce((sum, idea) => sum + idea.score, 0) / ideasCount :
          0;
        const monthName = previousMonth.toLocaleString('en-us', {month: 'long'});

        ideasData.push({
          month: monthName,
          value: ideasCount,
          score: averageScore,
        });
      }
      return ideasData.reverse();
    }

    static associate(models) {
      models.Company.belongsToMany(models.User, {
        through: {
          model: models.CompanyUser,
          unique: false,
        },
        foreignKey: 'companyId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'users',
      });

      models.Company.hasMany(models.Idea, {
        foreignKey: 'companyId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'ideas',
      });

      models.Company.hasMany(models.HardCriteria, {
        foreignKey: 'companyId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'hardCriteria',
      });

      models.Company.hasMany(models.SoftCriteria, {
        foreignKey: 'companyId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'softCriteria',
      });
    }
  };

  Company.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    encryptedPassword: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    encryptedPasswordIv: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    password: {
      type: DataTypes.VIRTUAL,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
      set(value) {
        this.setDataValue('password', value);
        const encryptedPassword = encrypt(value);
        this.setDataValue('encryptedPassword', encryptedPassword.content);
        this.setDataValue('encryptedPasswordIv', encryptedPassword.iv);
      },
      get() {
        const json = {
          iv: this.encryptedPasswordIv,
          content: this.encryptedPassword,
        };
        return decrypt(json);
      },
    },
    submissionPasswordRequired: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      validate: {
        notNull: false,
        notEmpty: false,
      },
    },
    setupComplete: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      validate: {
        notNull: false,
        notEmpty: false,
      },
    },
    authKey: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: true,
      validate: {
        notNull: false,
        notEmpty: false,
      },
    },
  }, {
    sequelize,
    modelName: 'Company',
  });

  return Company;
};
