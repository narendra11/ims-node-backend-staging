'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class CompanyUser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     * @param {models} models model.
     */
    static associate(models) {
      models.CompanyUser.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        as: 'user',
      });

      models.CompanyUser.belongsTo(models.Company, {
        foreignKey: 'companyId',
        onDelete: 'CASCADE',
        as: 'company',
      });
    }
  };
  CompanyUser.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    companyId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    role: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        notNull: false,
        notEmpty: false,
      },
    },
  }, {
    sequelize,
    modelName: 'CompanyUser',
  });

  return CompanyUser;
};
