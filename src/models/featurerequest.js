'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class FeatureRequest extends Model {
    static associate(models) {
      models.FeatureRequest.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        as: 'user',
      });
    }
  };
  FeatureRequest.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    subject: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Subject is required',
        },
        notEmpty: {
          args: true,
          msg: 'Subject is required',
        },
      },
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Description is required',
        },
        notEmpty: {
          args: true,
          msg: 'Description is required',
        },
      },
    },
  }, {
    sequelize,
    modelName: 'FeatureRequest',
  });
  // FeatureRequest.beforeCreate((featureRequest) => featureRequest.id = uuidv4());
  return FeatureRequest;
};
