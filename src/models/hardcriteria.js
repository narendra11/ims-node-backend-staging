'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class HardCriteria extends Model {
    static associate(models) {
      models.HardCriteria.belongsTo(models.Company, {
        foreignKey: 'companyId',
        onDelete: 'CASCADE',
        as: 'company',
      });

      models.HardCriteria.hasMany(models.HardCriteriaIdea, {
        foreignKey: 'hardCriteriaId',
        onDelete: 'CASCADE',
        as: 'hardCriteriaIdeas',
      });

      models.HardCriteria.belongsToMany(models.Idea, {
        through: {
          model: models.HardCriteriaIdea,
          unique: false,
        },
        foreignKey: 'hardCriteriaId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'idea',
      });
    }
  };
  HardCriteria.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    companyId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
  }, {
    sequelize,
    modelName: 'HardCriteria',
  });

  return HardCriteria;
};
