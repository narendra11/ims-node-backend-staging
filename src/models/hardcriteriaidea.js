'use strict';
const {Model} = require('sequelize');
const {v4: uuidv4} = require('uuid');

module.exports = (sequelize, DataTypes) => {
  class HardCriteriaIdea extends Model {
    static associate(models) {
      models.HardCriteriaIdea.belongsTo(models.Idea, {
        foreignKey: 'ideaId',
        onDelete: 'CASCADE',
        as: 'idea',
      });

      models.HardCriteriaIdea.belongsTo(models.HardCriteria, {
        foreignKey: 'hardCriteriaId',
        onDelete: 'CASCADE',
        as: 'hardCriteria',
      });
    }
  };

  HardCriteriaIdea.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    ideaId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    hardCriteriaId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    met: DataTypes.BOOLEAN,
  }, {
    sequelize,
    modelName: 'HardCriteriaIdea',
  });

  return HardCriteriaIdea;
};
