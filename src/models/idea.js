'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');
const stages = ['Unstarted', 'Customer Validation', 'Budgeting',
  'MVP', 'Disqualified', 'Completed'];

module.exports = (sequelize, DataTypes) => {
  class Idea extends Model {
    static associate(models) {
      models.Idea.belongsTo(models.Company, {
        foreignKey: 'companyId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'company',
      });

      models.Idea.belongsTo(models.Category, {
        foreignKey: 'categoryId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'category',
      });

      models.Idea.belongsToMany(models.HardCriteria, {
        through: {
          model: models.HardCriteriaIdea,
          unique: false,
        },
        foreignKey: 'ideaId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'hardCriteria',
      });

      models.Idea.belongsToMany(models.SoftCriteria, {
        through: {
          model: models.IdeaSoftCriteria,
          unique: false,
        },
        foreignKey: 'ideaId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'softCriteria',
      });

      models.Idea.hasMany(models.Attachment, {
        foreignKey: 'attachableId',
        constraints: false,
        scope: {
          attachableType: 'idea',
        },
        onDelete: 'CASCADE',
        as: 'files',
      });

      models.Idea.hasMany(models.HardCriteriaIdea, {
        foreignKey: 'ideaId',
        onDelete: 'CASCADE',
        as: 'hardCriteriaIdea',
      });

      models.Idea.hasMany(models.IdeaSoftCriteria, {
        foreignKey: 'ideaId',
        onDelete: 'CASCADE',
        as: 'ideaSoftCriteria',
      });
    }

    static stages() {
      return stages;
    }
  };
  Idea.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    companyId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    categoryId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    notes: {
      type: DataTypes.TEXT,
      allowNull: true,
      validate: {
        notNull: false,
        notEmpty: false,
      },
    },
    favorite: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      validate: {
        notNull: false,
        notEmpty: false,
      },
    },
    stage: {
      type: DataTypes.INTEGER,
      allowNull: true,
      validate: {
        notNull: false,
        notEmpty: false,
      },
      get() {
        const stageInteger = this.getDataValue('stage');
        return stageInteger ? stages[stageInteger] : null;
      },
      set(value) {
        const stageInteger = stages.indexOf(value);
        if (stageInteger != -1) this.setDataValue('stage', stageInteger);
      },
    },
    score: {
      type: DataTypes.INTEGER,
      allowNull: true,
      validate: {
        notNull: false,
        notEmpty: false,
      },
    },
  }, {
    sequelize,
    modelName: 'Idea',
  });

  return Idea;
};
