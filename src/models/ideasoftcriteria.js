'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class IdeaSoftCriteria extends Model {
    static associate(models) {
      models.IdeaSoftCriteria.belongsTo(models.Idea, {
        foreignKey: 'ideaId',
        onDelete: 'CASCADE',
        as: 'idea',
      });

      models.IdeaSoftCriteria.belongsTo(models.SoftCriteria, {
        foreignKey: 'softCriteriaId',
        onDelete: 'CASCADE',
        as: 'softCriteria',
      });
    }
  };
  IdeaSoftCriteria.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    ideaId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    softCriteriaId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    score: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'IdeaSoftCriteria',
  });
  return IdeaSoftCriteria;
};
