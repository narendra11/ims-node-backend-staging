'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');
const _ = require('lodash');
const weights = {1: 'low', 2: 'medium', 3: 'important'};

module.exports = (sequelize, DataTypes) => {
  class SoftCriteria extends Model {
    static associate(models) {
      models.SoftCriteria.belongsTo(models.Company, {
        foreignKey: 'companyId',
        onDelete: 'CASCADE',
        as: 'company',
      });

      models.SoftCriteria.hasMany(models.IdeaSoftCriteria, {
        foreignKey: 'softCriteriaId',
        onDelete: 'CASCADE',
        as: 'ideaSoftCriteria',
      });

      models.SoftCriteria.belongsToMany(models.Idea, {
        through: {
          model: models.IdeaSoftCriteria,
          unique: false,
        },
        foreignKey: 'softCriteriaId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'idea',
      });
    }

    static weightValue(name) {
      return Number(Object.keys(weights).find((key) => {
        return weights[key] === name.toLowerCase();
      }));
    }
  };

  SoftCriteria.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    companyId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    shortName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    weight: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
      get() {
        const weightInteger = this.getDataValue('weight');
        return weightInteger ? _.startCase(weights[weightInteger]) : null;
      },
      set(value) {
        const weightInteger = Number(Object.keys(weights).find((key) => {
          return weights[key] === value.toLowerCase();
        }));
        this.setDataValue('weight', weightInteger);
      },
    },
  }, {
    sequelize,
    modelName: 'SoftCriteria',
  });

  return SoftCriteria;
};
