'use strict';
const {v4: uuidv4} = require('uuid');
const SequelizeSlugify = require('sequelize-slugify');
const {Model} = require('sequelize');
const utils = require('../libs/utils');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      // define association here
      models.User.hasMany(models.AuthenticationToken, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
      });

      models.User.belongsToMany(models.Company, {
        through: {
          model: models.CompanyUser,
          unique: false,
        },
        foreignKey: 'userId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'companies',
      });

      models.User.hasMany(models.FeatureRequest, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
      });
    }

    company() {
      return this.companies[0] || null;
    }

    companyIdeas() {
      return this.company ? this.company.ideas : [];
    }
  };

  User.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'First Name is required',
        },
        notEmpty: {
          args: true,
          msg: 'First Name is required',
        },
      },
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Last Name is required',
        },
        notEmpty: {
          args: true,
          msg: 'Last Name is required',
        },
      },
    },
    slug: {
      type: DataTypes.STRING,
      unique: true,
    },
    fullName: {
      type: DataTypes.VIRTUAL,
      get() {
        return `${this.firstName} ${this.lastName}`;
      },
      set(value) {
        throw new Error('Do not try to set the `fullName` value!');
      },
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {
          args: true,
          msg: 'Email format is invalid',
        },
        notNull: {
          args: true,
          msg: 'Email is required',
        },
        notEmpty: {
          args: true,
          msg: 'Email is required',
        },
      },
    },
    phoneNumber: {
      type: DataTypes.STRING,
      allowNull: true,
      set(value) {
        const normalizedPhoneNumber = utils.normalizePhoneNumber(value);
        this.setDataValue('phoneNumber', normalizedPhoneNumber);
      },
      // validate: {
      //     notNull: true,
      //     notEmpty: true
      // }
    },
    role: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'user',
      validate: {
        notNull: true,
        notEmpty: true,
        isIn: [['admin', 'user']],
      },
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'active',
      validate: {
        notNull: true,
        notEmpty: true,
        isIn: {
          args: [['pending', 'active', 'inactive']],
          msg: 'Status not found',
        },
      },
    },
    resetPasswordToken: DataTypes.TEXT,
    resetPasswordSentAt: DataTypes.DATE,
    rememberCreatedAt: DataTypes.DATE,
    confirmationToken: DataTypes.TEXT,
    confirmationSentAt: DataTypes.DATE,
    confirmedAt: DataTypes.DATE,
    password: {
      type: DataTypes.VIRTUAL,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
      set(value) {
        this.setDataValue('password', value);
        const encryptedPassword = utils.hashPassword(value);
        this.setDataValue('encryptedPassword', encryptedPassword);
      },
    },
    encryptedPassword: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  }, {
    sequelize,
    modelName: 'User',
  });

  SequelizeSlugify.slugifyModel(User, {
    source: ['firstName', 'lastName'],
  });

  return User;
};
