const Validator = require('validatorjs');
const Models = require('../models');
const _ = require('lodash');

const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]/;

// Decamelize attribute names
Validator.setAttributeFormatter(function(attribute) {
  return _.startCase(attribute);
});

// Tighten password policy
Validator.register('strict', (value) => passwordRegex.test(value),
    'password must contain at least one uppercase letter, one lowercase letter and one number');

// unique
Validator.registerAsync('unique', function(value, attribute, req, passes) {
  if (!attribute) throw new Error('Specify Requirements i.e fieldName: exist:table,column');
  // split table and column
  const tableColumn = attribute.split(',');

  if (tableColumn.length !== 2) {
    throw new Error(`Invalid format for validation rule on ${attribute}`);
  }

  // assign array index 0 and 1 to table and column respectively
  const {
    0: table,
    1: column,
  } = tableColumn;

  // define custom error message
  const message = `${column} already in use`;

  // check if incoming value already exists in the database
  Models[table].unique({[column]: value})
      .then((result) => {
        if (result) {
          passes(false, message);
          return;
        }
        passes();
      });
});


const validator = (body, rules, customMessages, callback) => {
  const validation = new Validator(body, rules, customMessages);
  validation.passes(() => callback(null, true));
  validation.fails(() => callback(validation.errors, false));
};

module.exports = validator;
