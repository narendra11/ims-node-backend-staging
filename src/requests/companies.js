const Validator = require('./base');
const {HttpStatus} = require('../config/constants');
const utils = require('../libs/utils');

const auth = async (req, res, next) => {
  const validationRule = {
    'password': 'required|string',
  };

  await Validator(req.body.idea, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

module.exports = {
  auth,
};
