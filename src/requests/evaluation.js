const Validator = require('./base');
const {HttpStatus} = require('../config/constants');
const utils = require('../libs/utils');

const hardCriterium = async (req, res, next) => {
  const validationRule = {
    'met': 'required|string',
  };

  const message = {
    required: ':attribute is required',
  };

  await Validator(req.body.hardCriteria[0], validationRule, message, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

const softCriterium = async (req, res, next) => {
  const validationRule = {
    'score': 'required|integer',
  };

  const message = {
    required: ':attribute is required',
  };

  await Validator(req.body.softCriteria[0], validationRule, message, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

module.exports = {
  hardCriterium, softCriterium,
};
