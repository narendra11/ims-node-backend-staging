const Validator = require('./base');
const {HttpStatus} = require('../config/constants');
const utils = require('../libs/utils');

const update = async (req, res, next) => {
  let validationRule = {
    'first_name': 'string',
    'last_name': 'string',
    'email': 'string|email',
    'phone_number': 'string',
  };

  if (('password' in req.body.user)) {
    validationRule = {
      'current_password': 'required|string',
      'password': 'required|string|min:6|confirmed',
    };
  }

  const message = {
    required: ':attribute is required',
  };

  await Validator(req.body.user, validationRule, message, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

const signIn = async (req, res, next) => {
  const validationRule = {
    'email': 'required|string|email',
    'password': 'required|string',
  };

  const message = {
    required: ':attribute is required',
  };

  await Validator(req.body.user, validationRule, message, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

const emailAvailable = async (req, res, next) => {
  const validationRule = {
    'email': 'required|string|email',
  };
  await Validator(req.query, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

const forgotPassword = async (req, res, next) => {
  const validationRule = {
    'email': 'required|string|email',
  };
  await Validator(req.body.user, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

const resetPassword = async (req, res, next) => {
  const validationRule = {
    'password': 'required|string|min:6',
    'passwordConfirmation': 'required|same:password',
    'resetPasswordToken': 'required',
  };
  await Validator(req.body.user, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

module.exports = {
  signIn, emailAvailable, forgotPassword, resetPassword, update,
};
