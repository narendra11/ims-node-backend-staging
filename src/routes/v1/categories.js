const routes = require('express').Router();
const categories = require('../../controllers/v1/categories');

routes.route('/')
    .get(categories.index);

module.exports = routes;
