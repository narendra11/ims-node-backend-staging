const routes = require('express').Router();
const companies = require('../../controllers/v1/companies');
const authentication = require('../../middlewares/authentication');
const validation = require('../../requests/companies');

routes.route('/:id')
    .put(authentication, companies.update);

routes.route('/me')
    .get(authentication, companies.me);

routes.route('/my/dashboard')
    .get(authentication, companies.dashboard);

routes.route('/:companyUrl/auth_check')
    .get(companies.authCheck);

routes.route('/:companyUrl/auth')
    .post(validation.auth, companies.auth);

module.exports = routes;
