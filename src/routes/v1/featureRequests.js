const routes = require('express').Router();
const featureRequests = require('../../controllers/v1/featureRequests');
const authentication = require('../../middlewares/authentication');

routes.route('/')
    .post(authentication, featureRequests.create);

module.exports = routes;
