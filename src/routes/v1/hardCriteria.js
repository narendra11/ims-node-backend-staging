const routes = require('express').Router();
const hardCriteria = require('../../controllers/v1/hardCriteria');
const authentication = require('../../middlewares/authentication');

routes.route('/')
    .post(authentication, hardCriteria.create);

routes.route('/')
    .get(authentication, hardCriteria.index);

module.exports = routes;
