const routes = require('express').Router();
const ideas = require('../../controllers/v1/ideas');
const authentication = require('../../middlewares/authentication');
const validation = require('../../requests/evaluation');
const upload = require('../../services/file-upload');

routes.route('/')
    .post(upload.array('idea[image]'), rewriter, ideas.create);

routes.route('/new')
    .get(authentication, ideas.unscored);

routes.route('/')
    .get(authentication, ideas.index);

routes.route('/:id/evaluations')
    .get(authentication, ideas.evaluations);

routes.route('/:id/evaluations')
    .post(authentication, validation.hardCriterium, validation.softCriterium, ideas.evaluation);

routes.route('/:id')
    .put(authentication, ideas.update);

function rewriter(req, res, next) {
  // Set the request fields that you want
  req.body.idea.files = req.files;
  next();
}
module.exports = routes;
