const routes = require('express').Router();
const users = require('./users');
const hardCriteria = require('./hardCriteria');
const softCriteria = require('./softCriteria');
const companies = require('./companies');
const ideas = require('./ideas');
const featureRequests = require('./featureRequests');
const categories = require('./categories');

routes.get('/', (request, response) => {
  response.json({info: 'It works! IMS node.js API'}).status(200);
});

routes.use('/users', users);
routes.use('/hard_criteria', hardCriteria);
routes.use('/soft_criteria', softCriteria);
routes.use('/companies', companies);
routes.use('/ideas', ideas);
routes.use('/feature_requests', featureRequests);
routes.use('/categories', categories);

module.exports = routes;
