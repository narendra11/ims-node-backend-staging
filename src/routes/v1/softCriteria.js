const routes = require('express').Router();
const softCriteria = require('../../controllers/v1/softCriteria');
const authentication = require('../../middlewares/authentication');

routes.route('/')
    .post(authentication, softCriteria.create);

routes.route('/')
    .get(authentication, softCriteria.index);

module.exports = routes;
