const routes = require('express').Router();
const users = require('../../controllers/v1/users');
const sessions = require('../../controllers/v1/sessions');
const validation = require('../../requests/user');
const authentication = require('../../middlewares/authentication');
const password = require('../../controllers/v1/password');


routes.route('/sign_in')
    .post(validation.signIn, sessions.create);

routes.route('/sign_out')
    .delete(authentication, sessions.destroy);

routes.route('/current_user')
    .get(authentication, users.current);

routes.route('/')
    .put(authentication, users.update);

routes.route('/email_available')
    .get(validation.emailAvailable, users.emailAvailable);

routes.route('/password')
    .post(validation.forgotPassword, password.forgotPassword)
    .put(validation.resetPassword, password.resetPassword);

module.exports = routes;
