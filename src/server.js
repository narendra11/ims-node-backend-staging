const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/v1');
const cookieParser = require('cookie-parser');
const errorHandler = require('./libs/error_handler');
const helmet = require('helmet');
const app = express();
const path = require('path');
const cors = require('cors');
const inflector = require('json-inflector');
const viewEngine = require('express-json-views');
const viewHelpers = require('./views/helpers');
const morgan = require('morgan');
const paginate = require('express-paginate');
const {SlowRequestLog} = require('./models');

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}

app.set('x-powered-by', false);
app.enable('trust proxy');
app.disable('etag');

// Set view engine
app.engine('json', viewEngine({
  helpers: viewHelpers,
}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'json');

// cors allow
app.use(cors());
app.options('*', cors());

app.use(express.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(paginate.middleware(10, 50));

// Convert snake_case request keys to camelCase
app.use(inflector({request: 'camelizeLower', response: 'underscore'}));

app.use(helmet());
app.use(cookieParser());
app.use(morgan(function(tokens, req, res) {
  const method = tokens.method(req, res);
  const url = tokens.url(req, res);
  const status = tokens.status(req, res);
  const resContentLength = tokens.res(req, res, 'content-length');
  const responseTime = tokens['response-time'](req, res); // in ms
  const totalTime = tokens['total-time'](req, res); // in ms

  if (totalTime > (4 * 1000)) { // 4 seconds
    SlowRequestLog.create({
      method: method,
      url: url,
      status: status,
      resContentLength: resContentLength,
      responseTime: responseTime,
      TotalTime: totalTime,
    });
  }

  console.log(`${method} ${url} ${status} ${responseTime} ${totalTime}`);
  return JSON.stringify(req.body);
}));

// READ Request Handlers
app.use('/api/v1', routes);

// Error Handler
app.use(errorHandler.exceptionHandler);
app.use(errorHandler.clientHandler);


// PORT ENVIRONMENT VARIABLE
const port = process.env.PORT || 3005;
if (!module.parent) {
  app.listen(port, () => console.log(`Listening on port ${port}..`));
}

module.exports = app;
