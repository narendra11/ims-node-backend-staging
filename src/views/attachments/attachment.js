const attachmentView = (attachment) => {
  return {
    id: attachment.id,
    name: attachment.name,
    attachableId: attachment.attachableId,
    attachableType: attachment.attachableType,
    name: attachment.fileName,
    url: attachment.url,
    contentType: attachment.contentType,
    byteSize: attachment.byteSize,
  };
};

module.exports = attachmentView;
