const userView = require('../users/user');

const authenticationTokenView = (authenticationToken) => {
  const unscoredIdeasCount = authenticationToken.user.companyIdeas.length;

  return {
    authToken: authenticationToken.body,
    user: userView(authenticationToken.user, unscoredIdeasCount),
  };
};

module.exports = authenticationTokenView;
