const categoryView = (category) => {
  return {
    id: category.id,
    name: category.name,
  };
};

module.exports = categoryView;
