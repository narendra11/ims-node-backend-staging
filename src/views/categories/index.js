const categoryView = require('./category');

const categoriesIndexView = (categories) => {
  return categories.map((category) => categoryView(category));
};

module.exports = categoriesIndexView;
