const companyAuthView = (company) => {
  return {
    authKey: company.authKey,
  };
};

module.exports = companyAuthView;
