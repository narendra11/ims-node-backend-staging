const companyAuthCheckView = (company) => {
  return {
    submissionPasswordRequired: company.submissionPasswordRequired,
    setupComplete: company.setupComplete,
  };
};

module.exports = companyAuthCheckView;
