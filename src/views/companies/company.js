const companyView = (company) => {
  return {
    id: company.id,
    name: company.name,
    url: company.url,
    submissionPasswordRequired: company.submissionPasswordRequired,
    setupComplete: company.setupComplete,
  };
};

module.exports = companyView;
