const hardCriteriaIndexView = require('../hardCriteria/index');
const softCriteriaIndexView = require('../softCriteria/index');

const companyShowView = (company) => {
  return {
    id: company.id,
    name: company.name,
    url: company.url,
    submissionPasswordRequired: company.submissionPasswordRequired,
    setupComplete: company.setupComplete,
    password: company.password,
    authKey: company.authKey,
    hardCriteria: hardCriteriaIndexView(company.hardCriteria),
    softCriteria: softCriteriaIndexView(company.softCriteria),
  };
};

module.exports = companyShowView;
