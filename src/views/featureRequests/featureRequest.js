const featureRequestView = (featureRequest) => {
  return {
    id: featureRequest.id,
    userId: featureRequest.userId,
    subject: featureRequest.subject,
    description: featureRequest.description,
  };
};

module.exports = featureRequestView;
