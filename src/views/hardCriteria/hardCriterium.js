const hardCriteriumView = (hardCriterium) => {
  return {
    id: hardCriterium.id,
    companyId: hardCriterium.companyId,
    name: hardCriterium.name,
  };
};

module.exports = hardCriteriumView;
