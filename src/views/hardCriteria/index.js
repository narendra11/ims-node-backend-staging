const hardCriteriumView = require('./hardCriterium');

const hardCriteriaIndexView = (hardCriteria) => {
  return hardCriteria.map((hardCriterium) => hardCriteriumView(hardCriterium));
};

module.exports = hardCriteriaIndexView;
