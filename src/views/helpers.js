const helpers = {
  date: function(value, object) {
    if (!value) {
      return null;
    }
    return new Date(value).toISOString();
  },
};

module.exports = helpers;
