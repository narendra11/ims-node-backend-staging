const categoryView = require('../categories/category');
const attachmentView = require('../attachments/attachment');

const evaluationsView = (idea, company) => {
  return {
    idea: {
      id: idea.id,
      name: idea.name,
      description: idea.description,
      notes: idea.notes,
      submittedBy: `${idea.firstName} ${idea.lastName}`,
      createdAt: idea.createdAt,
      stage: idea.stage,
      score: idea.score,
      favorite: idea.favorite,
      category: categoryView(idea.category),
      files: idea.files.map((file) => attachmentView(file)),
    },
  };
};

module.exports = evaluationsView;
