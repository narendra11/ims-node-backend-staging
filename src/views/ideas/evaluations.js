// const companyView = require('../companies/company');
const categoryView = require('../categories/category');
const attachmentView = require('../attachments/attachment');
const hardCriteriumView = require('../hardCriteria/hardCriterium');
const softCriteriumView = require('../softCriteria/softCriterium');

const evaluationsView = (idea, company) => {
  return {
    idea: {
      id: idea.id,
      name: idea.name,
      description: idea.description,
      notes: idea.notes,
      submittedBy: `${idea.firstName} ${idea.lastName}`,
      createdAt: idea.createdAt,
      stage: idea.stage,
      score: idea.score,
      favorite: idea.favorite,
      company: company.id,
      hardCriteria: company.hardCriteria.map((hardCriterium) => {
        const hardCriteriumAttributes = hardCriteriumView(hardCriterium);
        hardCriteriumAttributes.met = hardCriterium.hardCriteriaIdeas[0] ?
                                      hardCriterium.hardCriteriaIdeas[0].met :
                                      null;
        return hardCriteriumAttributes;
      }),
      softCriteria: company.softCriteria.map((softCriterium) => {
        const softCriteriumAttributes = softCriteriumView(softCriterium);
        softCriteriumAttributes.score = softCriterium.ideaSoftCriteria[0] ?
                                        softCriterium.ideaSoftCriteria[0].score :
                                        null;
        return softCriteriumAttributes;
      }),
      // company: companyView(idea.company),
      category: categoryView(idea.category),
      files: idea.files.map((file) => attachmentView(file)),
    },
  };
};

module.exports = evaluationsView;
