const companyView = require('../companies/company');
const categoryView = require('../categories/category');
const attachmentView = require('../attachments/attachment');

const ideaView = (idea) => {
  return {
    id: idea.id,
    name: idea.name,
    description: idea.description,
    notes: idea.notes,
    submittedBy: `${idea.firstName} ${idea.lastName}`,
    createdAt: idea.createdAt,
    stage: idea.stage,
    score: idea.score,
    favorite: idea.favorite,
    company: companyView(idea.company),
    category: categoryView(idea.category),
    files: idea.files.map((file) => attachmentView(file)),
  };
};

module.exports = ideaView;
