const ideaView = require('./idea');

const ideasIndexView = (ideas) => {
  return {
    ideas: ideas.rows.map((idea) => ideaView(idea)),
    pageCount: ideas.pageCount,
    userHasIdeas: true,
  };

  ;
};

module.exports = ideasIndexView;
