const softCriteriumView = require('./softCriterium');

const softCriteriaIndexView = (softCriteria) => {
  return softCriteria.map((softCriterium) => softCriteriumView(softCriterium));
};

module.exports = softCriteriaIndexView;
