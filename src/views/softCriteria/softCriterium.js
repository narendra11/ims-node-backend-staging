const softCriteriumView = (softCriterium) => {
  return {
    id: softCriterium.id,
    companyId: softCriterium.companyId,
    name: softCriterium.name,
    shortName: softCriterium.shortName,
    weight: softCriterium.weight,
  };
};

module.exports = softCriteriumView;
