const userView = require('./user');

const userUpdateView = (user, token) => {
  return {
    user: userView(user),
    authToken: token,
  };
};

module.exports = userUpdateView;
