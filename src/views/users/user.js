const userView = (user, unscoredIdeasCount) => {
  const company = (user.companies && user.companies.length > 0) ?
    user.companies[0] :
    null;

  return {
    id: user.id,
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
    phoneNumber: user.phoneNumber,
    role: user.role,
    status: user.status,
    slug: user.slug,
    companyId: (company) ? company.id : null,
    setupComplete: (company) ? company.setupComplete : null,
    unscoredIdeas: unscoredIdeasCount,
  };
};

module.exports = userView;
